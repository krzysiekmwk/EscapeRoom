#include "HX711.h"

HX711 scale(A1, A0);    // parameter "gain" is ommited; the default value 128 is used by the library

byte outputPinLock = A2;

int weightToOpenPin = 110;
int weighError = 25;
int actualWeigh;

void setup() {
  Serial.begin(9600); 

  pinMode(outputPinLock, OUTPUT);

  digitalWrite(outputPinLock,HIGH);

  scale.set_scale(2280.f);                      // this value is obtained by calibrating the scale with known weights; see the README for details
  scale.tare();               // reset the scale to 0
}

void loop() {
  actualWeigh = scale.get_units(10);
  Serial.print("Weight: ");
  Serial.println(actualWeigh);

  int i = 0;
  while(abs(weightToOpenPin - actualWeigh) < weighError){
    actualWeigh = scale.get_units(10);
    Serial.print("Weight: ");
    Serial.println(actualWeigh);
    if(i == 4){
      Serial.println("CORRECT!");
      digitalWrite(outputPinLock,LOW);
      delay(10000);
      digitalWrite(outputPinLock,HIGH);
    }
    
    delay(1000);
    i++;
  }

  //scale.power_down();             // put the ADC in sleep mode
  delay(2500);
  //scale.power_up();
}
