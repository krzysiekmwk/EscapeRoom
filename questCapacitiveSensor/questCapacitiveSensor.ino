#include <CapacitiveSensor.h>

/*
 * CapitiveSense Library Demo Sketch
 * Paul Badger 2008
 * Uses a high value resistor e.g. 10M between send pin and receive pin
 * Resistor effects sensitivity, experiment with values, 50K - 50M. Larger resistor values yield larger sensor values.
 * Receive pin is the sensor pin - try different amounts of foil/metal on this pin
 */


//CapacitiveSensor   cs_2_4 = CapacitiveSensor(2,4);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_2_5 = CapacitiveSensor(2,5);        // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_2_6 = CapacitiveSensor(2,6);        // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_2_7 = CapacitiveSensor(2,7);        // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

byte outputPin = 10;

unsigned long previousMillis = 0;
const long interval = 10000;

void setup()                    
{
   //cs_2_5.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example
   //cs_2_6.set_CS_AutocaL_Millis(0xFFFFFFFF);
   //cs_2_7.set_CS_AutocaL_Millis(0xFFFFFFFF);
   Serial.begin(9600);
   pinMode(outputPin, OUTPUT);
   digitalWrite(outputPin, HIGH);
}

void loop()                    
{
    long start = millis();
    //long total1 =  cs_2_4.capacitiveSensor(30);
    long total2 =  cs_2_5.capacitiveSensor(35);
    long total3 =  cs_2_6.capacitiveSensor(35);
    long total4 =  cs_2_7.capacitiveSensor(35);

    Serial.print(millis() - start);        // check on performance in milliseconds
    //Serial.print("\t");                    // tab character for debug windown spacing

    //Serial.print(total1);                  // print sensor output 1
    
    Serial.print("\t");
    Serial.print(total2);                  // print sensor output 2
    Serial.print("\t");
    Serial.print(total3);                // print sensor output 3
    Serial.print("\t");
    Serial.println(total4);                // print sensor output 3

    if((total2 > 200) && (total3 > 200) && (total4 > 200)){
      digitalWrite(outputPin, LOW);
      delay(10000);
      digitalWrite(outputPin, HIGH);
      cs_2_5.reset_CS_AutoCal();
      cs_2_6.reset_CS_AutoCal();
      cs_2_7.reset_CS_AutoCal();
    }

    /*if (start - previousMillis >= interval) {
      previousMillis = start;
      cs_2_5.reset_CS_AutoCal();
      cs_2_6.reset_CS_AutoCal();
      cs_2_7.reset_CS_AutoCal();
    }*/

    

    delay(15);                             // arbitrary delay to limit data to serial port 
}
