class Relay {
 public:
  byte input;
  byte output;

  Relay(byte input, byte output){
    this->input = input;
    this->output = output;
    pinMode(input, INPUT_PULLUP);
    pinMode(output, OUTPUT);
    digitalWrite(output,HIGH);
  }

  int update(){
    if(digitalRead(input) == LOW)
      digitalWrite(output,LOW);
    else
      digitalWrite(output,HIGH);
  }
};

#define COUNT_OF_REALYS 8

Relay relays[COUNT_OF_REALYS] =
{
    Relay(2, 10),
    Relay(3, 11),
    Relay(4, 12),
    Relay(5, 13),
    Relay(6, A0),
    Relay(7, A1),
    Relay(8, A2),
    Relay(9, A3),
};

void setup() {
  Serial.begin(9600);
}

void loop() {
  for(int i = 0; i < COUNT_OF_REALYS; i++)
    relays[i].update();
}

