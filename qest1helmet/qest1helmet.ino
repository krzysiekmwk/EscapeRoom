#include <Bounce2.h>

#define NUM_BUTTONS 8

byte correctSequence = 0b00110101;//KOD od prawej strony w przyciskach. Czyli 2, 4, 6 i 7. Bo przyciski 1 i 0 zostawiam nie podlaczone
 
Bounce debouncer[NUM_BUTTONS];

byte actualButtonState[NUM_BUTTONS];

byte outputPinLock = A0;

void setup() {
  Serial.begin(9600);
  for(int i = 2; i < NUM_BUTTONS + 2; i++){
    pinMode(i,INPUT_PULLUP);
    debouncer[i-2].attach(i);
    debouncer[i-2].interval(15); // interval in ms
  }
  
  pinMode(outputPinLock,OUTPUT);

  digitalWrite(outputPinLock,HIGH);

  memset(actualButtonState,0,NUM_BUTTONS);

  for(int i = 0; i < NUM_BUTTONS; i++){
    actualButtonState[i] = !debouncer[i].read();
  }
}

void loop() {
  for(int i = 0; i < NUM_BUTTONS; i++){
    if(debouncer[i].update()){
      actualButtonState[i] = !debouncer[i].read();
  
      if(checkSequence()){
        digitalWrite(outputPinLock,LOW);
        delay(10000);
        digitalWrite(outputPinLock,HIGH);
      }
    }
  }
}

bool checkSequence(){
  byte firsByte = 0;

  for(int i = 0; i < 8; i++){
    firsByte |= (actualButtonState[i] << i);
  }
  Serial.println(firsByte, BIN);
  Serial.println(correctSequence, BIN);

  if(correctSequence == firsByte){
    Serial.println("Correct!");
    return true;
  }

  return false;
}

