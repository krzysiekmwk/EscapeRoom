#define outputA_1 2
#define outputB_1 3

int counter_1 = 0;
int aState_1;
int aLastState_1;

#define outputA_2 4
#define outputB_2 5

int counter_2 = 0;
int aState_2;
int aLastState_2;

#define outputA_3 6
#define outputB_3 7

int counter_3 = 0;
int aState_3;
int aLastState_3;

int correctPosition_1 = 80;
int correctPosition_2 = 20;
int correctPosition_3 = 70;

byte outputPinLock = A0;
int latachPin = A2;
int clockPin = A3;
int dataPin = A1;

void setup() {
  Serial.begin(9600);

  pinMode(outputA_1, INPUT_PULLUP);
  pinMode(outputB_1, INPUT_PULLUP);
  pinMode(outputA_2, INPUT_PULLUP);
  pinMode(outputB_2, INPUT_PULLUP);
  pinMode(outputA_3, INPUT_PULLUP);
  pinMode(outputB_3, INPUT_PULLUP);

  aLastState_1 = digitalRead(outputA_1);
  aLastState_2 = digitalRead(outputA_2);
  aLastState_3 = digitalRead(outputA_3);

  pinMode(latachPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(outputPinLock,OUTPUT);
  digitalWrite(outputPinLock,HIGH);

  registerWrite(counter_1, counter_2, counter_3);
}

void loop() {
  aState_1 = digitalRead(outputA_1);
  if(aState_1 != aLastState_1){
    if(digitalRead(outputB_1) != aState_1){
      counter_1--;
      if(counter_1 < 0)
        counter_1 = 0;
    }
    else{
      counter_1++;
      if(counter_1 > 89)
        counter_1 = 89;
    }

    Serial.print("Position1: ");
    Serial.println(counter_1);
  }

  aState_2 = digitalRead(outputA_2);
  if(aState_2 != aLastState_2){
    if(digitalRead(outputB_2) != aState_2){
      counter_2--;
      if(counter_2 < 0)
        counter_2 = 0;
    }
    else{
      counter_2++;
      if(counter_2 > 89)
        counter_2 = 89;
    }

    Serial.print("Position2: ");
    Serial.println(counter_2);
  }

  aState_3 = digitalRead(outputA_3);
  if(aState_3 != aLastState_3){
    if(digitalRead(outputB_3) != aState_3){
      counter_3--;
      if(counter_3 < 0)
        counter_3 = 0;
    }
    else{
      counter_3++;
      if(counter_3 > 89)
        counter_3 = 89;
    }

    Serial.print("Position3: ");
    Serial.println(counter_3);
  }

  aLastState_1 = aState_1;
  aLastState_2 = aState_2;
  aLastState_3 = aState_3;

  registerWrite(counter_1, counter_2, counter_3);

  if((counter_1 - correctPosition_1 >= 0) && (counter_1 - correctPosition_1 < 10) &&
  (counter_2 - correctPosition_2 >= 0) && (counter_2 - correctPosition_2 < 10) &&
  (counter_3 - correctPosition_3 >= 0) && (counter_3 - correctPosition_3 < 10)){
    Serial.print("CORRECT!");
    digitalWrite(outputPinLock,LOW);
    delay(10000);
    digitalWrite(outputPinLock,HIGH);
    counter_1 = 0;
    counter_2 = 0;
    counter_3 = 0;
  }

}

void registerWrite(byte pipe_1, byte pipe_2, byte pipe_3){
  byte firsByte = 0;
  byte secondByte = 0;
  byte thirdByte = 0;
  
  for(int i = 0; i < pipe_1/10; i++){
    firsByte |= 1 << i;
  }

  for(int i = 0; i < pipe_2/10; i++){
    secondByte |= 1 << i;
  }

  for(int i = 0; i < pipe_3/10; i++){
    thirdByte |= 1 << i;
  }

  Serial.println(firsByte, BIN);
  Serial.println(secondByte, BIN);
  Serial.println(thirdByte, BIN);
  digitalWrite(latachPin,LOW);
  shiftOut(dataPin,clockPin,MSBFIRST,thirdByte);
  shiftOut(dataPin,clockPin,MSBFIRST,secondByte);
  shiftOut(dataPin,clockPin,MSBFIRST,firsByte);
  digitalWrite(latachPin,HIGH);
}

