#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);

int menuState = 0;

void setup() {
  mySoftwareSerial.begin(9600);
  Serial.begin(9600);
  
  pinMode(2, INPUT_PULLUP); //Zagadka 1
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP); //RESET

  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT);
  digitalWrite(9,LOW);

  delay(5000);

  while (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    delay(1000);
  }

  //myDFPlayer.reset();
  //myDFPlayer.outputDevice(DFPLAYER_DEVICE_AUX);
  //myDFPlayer.enableDAC();

  myDFPlayer.volume(30);  //Set volume value. From 0 to 30
  myDFPlayer.next();
  myDFPlayer.pause();
  myDFPlayer.loop(1);
  Serial.println("Gram pierwsza muzyke w petli i ELO.");
  Serial.println(myDFPlayer.readVolume());
}

void loop() {
  
  if((digitalRead(2) == LOW) && menuState == 0){
    Serial.println("Muzyka 1 sie palzuje");
    Serial.println("Granie kawalka po otworzeniu drzwi 1");
    myDFPlayer.pause();
    myDFPlayer.play(4);
    delay(9000);
    Serial.println("Granie muzyki 2 w petli");
    myDFPlayer.pause();
    myDFPlayer.loop(2);
    menuState++;
  }

  if((digitalRead(3) == LOW) && menuState == 1){
    Serial.println("Muzyka 2 sie palzuje");
    Serial.println("Granie kawalka po otworzeniu drzwi 2");
    myDFPlayer.pause();
    myDFPlayer.play(5);
    delay(3000);
    Serial.println("Granie muzyki 3 w petli");
    myDFPlayer.pause();
    myDFPlayer.loop(3);
    menuState++;
  }

  if((digitalRead(4) == LOW) && menuState == 2){
    Serial.println("Muzyka 3 sie palzuje");
    Serial.println("Granie kawalka po otworzeniu drzwi3");
    Serial.println("Granie muzyki 4 w petli");
    menuState = 3;
    myDFPlayer.pause();
    myDFPlayer.loop(1);
  }

  if(digitalRead(5) == LOW){
    Serial.println("Reset");
    menuState = 0;
    myDFPlayer.pause();
    myDFPlayer.loop(1);
  }

  if(digitalRead(8) == LOW){
    Serial.println("BOOM");
    myDFPlayer.pause();
    myDFPlayer.play(6);
    delay(7000);
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    menuState = 0;
  }

  if(digitalRead(9) == HIGH  && menuState == 3){
    Serial.println("FIGURKA");
    myDFPlayer.pause();
    myDFPlayer.play(7);
    delay(17000);
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    menuState = 0;
  }

  if (myDFPlayer.available()) {
    printDetail(myDFPlayer.readType(), myDFPlayer.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
  
}


void printDetail(uint8_t type, int value){
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value);
      Serial.println(F(" Play Finished!"));
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
