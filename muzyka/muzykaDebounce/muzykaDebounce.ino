#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include <Bounce2.h>

SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);

Bounce buttonZloto = Bounce(); 
Bounce buttonPokretla = Bounce(); 
Bounce buttonGornicy = Bounce(); 
Bounce buttonDynamit = Bounce(); 
Bounce buttonReset = Bounce(); 

int czyZloto = 0;
int czyPokretla = 0;
int czyGornicy = 0;
int czyDynamit = 0;

void setup() {
  mySoftwareSerial.begin(9600);
  Serial.begin(9600);
  Serial.println("Ustawianie Pinow");
  pinMode(2, INPUT_PULLUP); //Zagadka Zloto
  pinMode(3, INPUT_PULLUP); //Zagadka Pokretla
  pinMode(4, INPUT_PULLUP); //Zagadka Gornicy
  pinMode(5, INPUT_PULLUP); //Zagadka Dynamit
  pinMode(6, INPUT_PULLUP); //RESET

  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH);

  buttonZloto.attach(2);
  buttonPokretla.attach(3);
  buttonGornicy.attach(4);
  buttonDynamit.attach(5);
  buttonReset.attach(6);

  buttonZloto.interval(15);
  buttonPokretla.interval(15);
  buttonGornicy.interval(15);
  buttonDynamit.interval(15);
  buttonReset.interval(15);

  delay(3000);

  buttonZloto.update();
  buttonPokretla.update();
  buttonGornicy.update();
  buttonDynamit.update();
  buttonReset.update();

  delay(2000);

  while (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    delay(1000);
  }

  Serial.println("Start");
  myDFPlayer.volume(30);  //Set volume value. From 0 to 30
  myDFPlayer.next();
  myDFPlayer.pause();
  myDFPlayer.loop(1);
  Serial.println("Muzyka w petli (1)");
}

void loop() {

  buttonZloto.update();
  buttonPokretla.update();
  buttonGornicy.update();
  buttonDynamit.update();
  bool wasReset = buttonReset.update();
  
  if((buttonZloto.read() == HIGH) && czyZloto == 0){
    Serial.println("Muzyka (1) pauza");
    Serial.println("Glos ducha (1)");
    myDFPlayer.pause();
    myDFPlayer.play(2);
    delay(17000);
    Serial.println("Muzyka w petli (1) lub (2) //zalezy od aktualnej granej");
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    czyZloto = 1;
  }

  if((buttonPokretla.read() == LOW) && czyPokretla == 0){
    Serial.println("Muzyka (1) pauza");
    Serial.println("Glos ducha (2)");
    myDFPlayer.pause();
    myDFPlayer.play(3);
    delay(10000);
    Serial.println("Muzyka w petli (2)");
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    czyPokretla = 1;
  }

  if((buttonGornicy.read() == LOW) && czyGornicy == 0){
    Serial.println("Muzyka (2) pauza");
    Serial.println("Glos ducha (3)");
    myDFPlayer.pause();
    myDFPlayer.play(4);
    delay(13000);
    Serial.println("Muzyka w petli (2)");
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    czyGornicy = 1;
  }

  if((buttonDynamit.read() == LOW) && czyDynamit == 0){
    Serial.println("Muzyka (2) pauza");
    Serial.println("Glos ducha (4)");
    digitalWrite(9, LOW);
    myDFPlayer.pause();
    myDFPlayer.play(7);
    delay(4000);
    myDFPlayer.pause();
    myDFPlayer.play(5);
    delay(12000);
    myDFPlayer.pause();
    myDFPlayer.loop(1);
    digitalWrite(9, HIGH);
    czyDynamit = 1;
  }

  if((buttonReset.read() == LOW) && wasReset){
    Serial.println("RESET");
    czyZloto = 0;
    czyPokretla = 0;
    czyGornicy = 0;
    czyDynamit = 0;
    myDFPlayer.pause();
    myDFPlayer.play(6);
    delay(25000);
    myDFPlayer.pause();
    myDFPlayer.loop(1);
  }

  if (myDFPlayer.available()) {
    printDetail(myDFPlayer.readType(), myDFPlayer.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
  
}


void printDetail(uint8_t type, int value){
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value);
      Serial.println(F(" Play Finished!"));
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
