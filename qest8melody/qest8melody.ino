//Correct button for melody: 2 0 1 3. Czyli rzeczywiście 4 2 3 5. 6 sluzy do udsluchania calej muzyczki

#include <Bounce2.h>

#define NUM_BUTTONS 5
 
Bounce debouncer[NUM_BUTTONS];

byte speakerOutput = 9;

byte goodPass = 0;

byte outputPinLock = 8;

void setup() {
  Serial.begin(9600);
  for(int i = 2; i < NUM_BUTTONS + 2; i++){
    pinMode(i,INPUT_PULLUP);
    debouncer[i-2].attach(i);
    debouncer[i-2].interval(15); // interval in ms
  }
  
  pinMode(speakerOutput,OUTPUT);
  pinMode(outputPinLock, OUTPUT);

  digitalWrite(outputPinLock,HIGH);
}

void loop() {
  for(int i = 0; i < NUM_BUTTONS; i++){
    if(debouncer[i].update()){
      if(debouncer[i].fell()){
        
        if(i == 0){
          tone(speakerOutput, 1568, 500);

          if(goodPass == 0b00001111)
            goodPass |= 1 << 4;
          else{
            if(goodPass != 0){
              tone(speakerOutput, 50, 2500);
              delay(2500);
            }
            goodPass = 0;
          }
          delay(500);
        }
          
        if(i == 1){
          tone(speakerOutput, 4978, 500);

          if(goodPass == 0b00000011)
            goodPass |= 1 << 2;
          else{
            if(goodPass != 0){
              tone(speakerOutput, 50, 2500);
              delay(2500);
            }
            goodPass = 0;
          }
          delay(500);
        }
          
        if(i == 2){
          tone(speakerOutput, 988, 500);

          if(goodPass == 0b00000000)
            goodPass |= 1 << 0;
          else if(goodPass == 0b00000111)
            goodPass |= 1 << 3;
          else{
            if(goodPass != 0){
              tone(speakerOutput, 50, 2500);
              delay(2500);
            }
            goodPass = 0;
          }
          delay(500);
        }
          
        if(i == 3){
          tone(speakerOutput, 31, 500);

          if(goodPass == 0b00000001)
            goodPass |= 1 << 1;
          else if(goodPass == 0b00011111)
            goodPass |= 1 << 5;
          else{
            if(goodPass != 0){
              tone(speakerOutput, 50, 2500);
              delay(2500);
            }
            goodPass = 0;
          }
          delay(500);
        }
          
        if(i == 4){ //Play all melody
          tone(speakerOutput, 988, 500);
          delay(1000);
          tone(speakerOutput, 31, 500);
          delay(1000);
          tone(speakerOutput, 4978, 500);
          delay(1000);
          tone(speakerOutput, 988, 500);
          delay(1000);
          //tone(speakerOutput, 1568, 500);
          //delay(1000);
          //tone(speakerOutput, 31, 500);
          //delay(1000);
        }

        Serial.println(goodPass, BIN);
        if(goodPass == 0b00001111){
          Serial.println("Correct music!");
          digitalWrite(outputPinLock,LOW);
          delay(10000);
          digitalWrite(outputPinLock,HIGH);
          goodPass = 0;
        }
      }
    }
  }
}

