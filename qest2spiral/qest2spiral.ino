#include <Bounce2.h>

#define NUM_BUTTONS 7
 
Bounce debouncer[NUM_BUTTONS];

byte speakerOutput = 9;

void setup() {
  Serial.begin(9600);
  for(int i = 2; i < NUM_BUTTONS + 2; i++){
    pinMode(i,INPUT_PULLUP);
    debouncer[i-2].attach(i);
    debouncer[i-2].interval(15); // interval in ms
  }
  
  pinMode(speakerOutput,OUTPUT);
}

void loop() {
  for(int i = 0; i < NUM_BUTTONS; i++){
    debouncer[i].update();
    if(!debouncer[i].read()){
      tone(speakerOutput,1000, 100);
      delay(300);
    }
  }
}

