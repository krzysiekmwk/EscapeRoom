#include <Rotary.h>

int counter_1 = 0;
int counter_2 = 0;
int counter_3 = 0;

Rotary firstEncoder = Rotary(2, 3);
Rotary secondEncoder = Rotary(4, 5);
Rotary thirdEncoder = Rotary(6, 7);

int correctPosition_1 = 80;
int correctPosition_2 = 30;
int correctPosition_3 = 60;

byte outputPinLock = A0;
int latachPin = A2;
int clockPin = A3;
int dataPin = A1;

int soundPin = 9;

void setup() {
  Serial.begin(9600);

  pinMode(latachPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(outputPinLock,OUTPUT);
  digitalWrite(outputPinLock,HIGH);

  registerWrite(counter_1, counter_2, counter_3);
}

void loop() {
  unsigned char result = firstEncoder.process();
  if (result) {
    tone(soundPin, 2000, 40);
    delay(5);
    tone(soundPin, 50, 30);
    if(result == DIR_CW){
      counter_1--;
      if(counter_1 < 0)
        counter_1 = 0;
    }
    else{
      counter_1++;
      if(counter_1 > 89)
        counter_1 = 89;
    }

    Serial.print("Position1: ");
    Serial.println(counter_1);

    registerWrite(counter_1, counter_2, counter_3);
  }

  result = secondEncoder.process();
  if (result) {
    tone(soundPin, 2000, 40);
    delay(5);
    tone(soundPin, 50, 30);
    if(result == DIR_CW){
      counter_2--;
      if(counter_2 < 0)
        counter_2 = 0;
    }
    else{
      counter_2++;
      if(counter_2 > 89)
        counter_2 = 89;
    }

    Serial.print("Position2: ");
    Serial.println(counter_2);

    registerWrite(counter_1, counter_2, counter_3);
  }

  result = thirdEncoder.process();
  if (result) {
    tone(soundPin, 2000, 40);
    delay(5);
    tone(soundPin, 50, 30);
    if(result == DIR_CW){
      counter_3--;
      if(counter_3 < 0)
        counter_3 = 0;
    }
    else{
      counter_3++;
      if(counter_3 > 89)
        counter_3 = 89;
    }

    Serial.print("Position3: ");
    Serial.println(counter_3);

    registerWrite(counter_1, counter_2, counter_3);
  }

  if((counter_1 - correctPosition_1 >= 0) && (counter_1 - correctPosition_1 < 10) &&
  (counter_2 - correctPosition_2 >= 0) && (counter_2 - correctPosition_2 < 10) &&
  (counter_3 - correctPosition_3 >= 0) && (counter_3 - correctPosition_3 < 10)){
    Serial.print("CORRECT!");
    digitalWrite(outputPinLock,LOW);
    delay(10000);
    digitalWrite(outputPinLock,HIGH);
    counter_1 = 0;
    counter_2 = 0;
    counter_3 = 0;

    registerWrite(counter_1, counter_2, counter_3);
  }

}

void registerWrite(byte pipe_1, byte pipe_2, byte pipe_3){
  byte firsByte = 0;
  byte secondByte = 0;
  byte thirdByte = 0;
  
  for(int i = 0; i < pipe_1/10; i++){
    firsByte |= 1 << i;
  }

  for(int i = 0; i < pipe_2/10; i++){
    secondByte |= 1 << i;
  }

  for(int i = 0; i < pipe_3/10; i++){
    thirdByte |= 1 << i;
  }

  Serial.println(firsByte, BIN);
  Serial.println(secondByte, BIN);
  Serial.println(thirdByte, BIN);
  digitalWrite(latachPin,LOW);
  shiftOut(dataPin,clockPin,MSBFIRST,thirdByte);
  shiftOut(dataPin,clockPin,MSBFIRST,secondByte);
  shiftOut(dataPin,clockPin,MSBFIRST,firsByte);
  digitalWrite(latachPin,HIGH);
}

