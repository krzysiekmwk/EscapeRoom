#include <Bounce2.h>

#define NUM_BUTTONS 12

//CODE is 8 2 11 in button
#define FIRST_CORRECT_BUTTON 7
#define SECOND_CORRECT_BUTTON 1
#define THIRD_CORRECT_BUTTON 10
 
Bounce debouncer[NUM_BUTTONS];

byte steps = 0;
byte actualButtonState[NUM_BUTTONS];

byte goodPass = 0;

byte outputPinLock = A0;
int latachPin = A2;
int clockPin = A3;
int dataPin = A1;

void setup() {
  Serial.begin(9600);
  for(int i = 2; i < NUM_BUTTONS + 2; i++){
    pinMode(i,INPUT_PULLUP);
    debouncer[i-2].attach(i);
    debouncer[i-2].interval(15); // interval in ms
  }

  pinMode(0,INPUT_PULLUP);
  debouncer[11].attach(0);
  debouncer[11].interval(15);
  
  pinMode(outputPinLock,OUTPUT);
  pinMode(latachPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  digitalWrite(outputPinLock,HIGH);

  memset(actualButtonState,0,NUM_BUTTONS);
  delay(500);
  registerWrite();
}

void loop() {
  for(int i = 0; i < NUM_BUTTONS; i++){
    if(debouncer[i].update()){
      if(debouncer[i].fell()){
        Serial.print("change! ");
        Serial.print(i);
        Serial.print(" step: ");
        Serial.print(steps);

        if(actualButtonState[i] != 1){
          actualButtonState[i] = 1;

          if(steps == 0 && actualButtonState[FIRST_CORRECT_BUTTON] == 1)
            goodPass++;
          
          if(steps == 1 && actualButtonState[SECOND_CORRECT_BUTTON] == 1)
            goodPass++;
          
          if(steps == 2 && actualButtonState[THIRD_CORRECT_BUTTON] == 1)
            goodPass++;

            //Serial.println(goodPass);

            if(steps == 2){
              if(goodPass == 3){
                Serial.println("PASS IS GOOD");
                digitalWrite(outputPinLock,LOW);//Jesli haslo jest poprawne, daj stan niski na lini
                memset(actualButtonState,1,NUM_BUTTONS);
                registerWrite();
                delay(10000);
                digitalWrite(outputPinLock,HIGH);
              }
                goodPass = 0;
                steps = -1;
                registerWrite();
                delay(1000);
                memset(actualButtonState,0,NUM_BUTTONS);
            }
          for(int i = 0; i < NUM_BUTTONS; i++){
            Serial.print(actualButtonState[i]);
          }
          Serial.println();
          registerWrite();
          
          steps++;
        }
      }
    }
  }
}

void registerWrite(){
  byte firsByte = 0;
  byte secondByte = 0;

  for(int i = 0; i < 8; i++){
    firsByte |= (actualButtonState[i] << i);
  }
  for(int i = 8, j = 0; i < NUM_BUTTONS; i++, j++){
    secondByte ^= (actualButtonState[i] << j);
  }

  //secondByte ^= secondByte;
  Serial.println(firsByte, BIN);
  Serial.println(secondByte, BIN);
  digitalWrite(latachPin,LOW);
  shiftOut(dataPin,clockPin,MSBFIRST,secondByte);
  shiftOut(dataPin,clockPin,MSBFIRST,firsByte);
  digitalWrite(latachPin,HIGH);
}

