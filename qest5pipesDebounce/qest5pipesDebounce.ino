#include <Bounce2.h>

#define outputA_1 2
#define outputB_1 3
Bounce inputA_1;
Bounce inputB_1;

int counter_1 = 0;
int aState_1;
int aLastState_1;

#define outputA_2 4
#define outputB_2 5
Bounce inputA_2;
Bounce inputB_2;

int counter_2 = 0;
int aState_2;
int aLastState_2;

#define outputA_3 6
#define outputB_3 7
Bounce inputA_3;
Bounce inputB_3;

int counter_3 = 0;
int aState_3;
int aLastState_3;

int correctPosition_1 = 80;
int correctPosition_2 = 20;
int correctPosition_3 = 70;

byte outputPinLock = A0;
byte latachPin = A4;
byte clockPin = A3;
byte dataPin = A5;

byte speakerPin = 8;

void setup() {
  Serial.begin(9600);

  pinMode(outputA_1, INPUT_PULLUP);
  pinMode(outputB_1, INPUT_PULLUP);
  pinMode(outputA_2, INPUT_PULLUP);
  pinMode(outputB_2, INPUT_PULLUP);
  pinMode(outputA_3, INPUT_PULLUP);
  pinMode(outputB_3, INPUT_PULLUP);

  inputA_1.attach(outputA_1);
  inputB_1.attach(outputB_1);
  inputA_2.attach(outputA_2);
  inputB_2.attach(outputB_2);
  inputA_3.attach(outputA_3);
  inputB_3.attach(outputB_3);

  inputA_1.interval(15);
  inputB_1.interval(15);
  inputA_2.interval(15);
  inputB_2.interval(15);
  inputA_3.interval(15);
  inputB_3.interval(15);

  inputA_1.update();
  inputB_1.update();
  inputA_2.update();
  inputB_2.update();
  inputA_3.update();
  inputB_3.update();

  aLastState_1 = inputA_1.read();
  aLastState_2 = inputA_2.read();
  aLastState_3 = inputA_3.read();

  pinMode(latachPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(outputPinLock,OUTPUT);
  digitalWrite(outputPinLock,HIGH);
  pinMode(speakerPin, OUTPUT);

  registerWrite(counter_1, counter_2, counter_3);
}

void loop() {
  aState_1 = inputA_1.read();
  if(aState_1 != aLastState_1){
    if(inputB_1.read() != aState_1){
      counter_1--;
      playTone();
      if(counter_1 < 0)
        counter_1 = 0;
    }
    else{
      counter_1++;
      playTone();
      if(counter_1 > 89)
        counter_1 = 89;
    }

    Serial.print("Position1: ");
    Serial.println(counter_1);
  }

  aState_2 = inputA_2.read();
  if(aState_2 != aLastState_2){
    if(inputB_2.read() != aState_2){
      counter_2--;
      playTone();
      if(counter_2 < 0)
        counter_2 = 0;
    }
    else{
      counter_2++;
      playTone();
      if(counter_2 > 89)
        counter_2 = 89;
    }

    Serial.print("Position2: ");
    Serial.println(counter_2);
  }

  aState_3 = inputA_3.read();
  if(aState_3 != aLastState_3){
    if(inputB_3.read() != aState_3){
      counter_3--;
      playTone();
      if(counter_3 < 0)
        counter_3 = 0;
    }
    else{
      counter_3++;
      playTone();
      if(counter_3 > 89)
        counter_3 = 89;
    }

    Serial.print("Position3: ");
    Serial.println(counter_3);
  }

  aLastState_1 = aState_1;
  aLastState_2 = aState_2;
  aLastState_3 = aState_3;

  registerWrite(counter_1, counter_2, counter_3);

  if((counter_1 - correctPosition_1 >= 0) && (counter_1 - correctPosition_1 < 10) &&
  (counter_2 - correctPosition_2 >= 0) && (counter_2 - correctPosition_2 < 10) &&
  (counter_3 - correctPosition_3 >= 0) && (counter_3 - correctPosition_3 < 10)){
    Serial.print("CORRECT!");
    digitalWrite(outputPinLock,LOW);
    delay(10000);
    digitalWrite(outputPinLock,HIGH);
    counter_1 = 0;
    counter_2 = 0;
    counter_3 = 0;
  }

  inputA_1.update();
  inputB_1.update();
  inputA_2.update();
  inputB_2.update();
  inputA_3.update();
  inputB_3.update();
}

void registerWrite(byte pipe_1, byte pipe_2, byte pipe_3){
  byte firsByte = 0;
  byte secondByte = 0;
  byte thirdByte = 0;
  
  for(int i = 0; i < pipe_1/10; i++){
    firsByte |= 1 << i;
  }

  for(int i = 0; i < pipe_2/10; i++){
    secondByte |= 1 << i;
  }

  for(int i = 0; i < pipe_3/10; i++){
    thirdByte |= 1 << i;
  }

  digitalWrite(latachPin,LOW);
  shiftOut(dataPin,clockPin,MSBFIRST,thirdByte);
  shiftOut(dataPin,clockPin,MSBFIRST,secondByte);
  shiftOut(dataPin,clockPin,MSBFIRST,firsByte);
  digitalWrite(latachPin,HIGH);
}

void playTone(){
  tone(speakerPin, 3800, 20);
  delay(3);
  tone(speakerPin, 1800, 10);
}

